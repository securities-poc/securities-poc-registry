FROM azul/zulu-openjdk-alpine:11.0.5-jre
MAINTAINER olarinde.ajai@gmail.com
COPY target/securities-poc-registry.jar /opt
WORKDIR /opt

ENV JAVA_OPTS Xmx2048 Xms1024
ENV APP_SECURITIES_POC_REGISTRY_SERVER_PORT 7000

ENTRYPOINT ["java", "-jar", "securities-poc-registry.jar"]
